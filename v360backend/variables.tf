#
#  See https://www.terraform.io/intro/getting-started/variables.html for more details.
#

#  Change these defaults to fit your needs!

variable "esxi_hostname" {
  default = "10.0.40.62"
}

variable "esxi_hostport" {
  default = "22"
}

variable "esxi_hostssl" {
  default = "443"
}

variable "esxi_username" {
  default = "root"
}

variable "esxi_password" {
  default = "VMzxc!123" # Unspecified will prompt
}

# variable "virtual_network" {
#   default = "VLAN2"
# }

variable "disk_store" {
  default = "SSDVMsRAID5"
}


variable "ovf_file" {
  #  A local file downloaded from https://cloud-images.ubuntu.com
  #default = "ubuntu-19.04-server-cloudimg-amd64.ova"

  #  Or specify a remote (url) file
  default = "https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.ova"
}

variable "vm_hostname" {
  default = "ubuntu-18.04-v360backend"
}
