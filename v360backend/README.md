Terraform is an infrastructure as code tool that lets you build, change, and version infrastructure safely and efficiently. This includes low-level components like compute instances, storage, and networking, as well as high-level components like DNS entries and SaaS features.
```
The VMs resources are specified in main.tf file
All variables are stored in variables.tf file.
Version of using terraform providers are described in versions.tf file
The gitlab pipeline runs several jobs: init(provider) plan(save output) apply(manual)
To build an VM try to run gitlab pipeline.
```