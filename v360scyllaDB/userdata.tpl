#cloud-config


#  Install stuff
packages:
 - ntp
 - ntpdate
 - curl
 #- wget

# Override ntp with chrony configuration on Ubuntu
ntp:
  enabled: true
  ntp_client: chrony  # Uses cloud-init default chrony configuration


# Configure ubuntu user security
users:
  - name: root
    sudo: [ "ALL=(ALL) NOPASSWD:ALL" ]
    #groups: docker
    ssh-authorized-keys:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDjn5jBCw/KdUymKkYS6mhXr+mjhrxzjFxzjfQ4FueNG9PBLqIaCGOYbjHtvMUfR1SfG2f+idCvWC31AJOJCALb8OTa6viXZKHjWIWnSrJJDW5hM4gjncwPsmVL4b2PEFngHG85hHBWP2ljYf0VrzcUntCEebF82k1vUkYQ3HOL5NYipWa6FQgRg8WucuXOxN+PRdpGs3WXUwY87ByiOgu8ql+FG3uktoKqBVSQnbsuxPeebs+yFvxyqiz3/m0XChKr3X/fGybWd5bYKvp3G6z5//HNqe4h82/GA2uO1wdpkgCBtah780knMMfeB5XOkpq5zRnNZ4/awx/AamcLDSPzCYnM2gTRYWQkaKI3W/uTiYrIo4ftRKJUQSY2CKAIfx/VIBJRFgWlArdHgKRAMJTEFMqQIbc3rHARXzQoDDe6Y58x8bfVbSJkFMEHYtjk+7QUjWzFLKy1NQmzsSv00qUVjhqMsu72jIrJz+cZxhJy69Z61MIVZiRTtgDQOefT++k= www@www
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDFiP3HGNoSze3gKKxVuThGcxLSGpkUO25/0ovU/CfwbfqiDb73WUcebpMbAnd6Lldf/L3pKez+uS90uJlurCfE2Gc1TVSpGSj2Y4rx1gosUzBLxefeMtVZ9HIayVNcwQnX4jGNWm7cBA+hlBnHuEc4JJ1ufi8LdIfuTwhjwRbJZdA635Vn8+hR464PCIHKm8XeVsPcUGfF+sDbO6nBXvzJIrWk+L5zznW5VtnJcMYVRWQAj8/dfVu/JiDuw2jHblT0VG/f1m+HsCmhkO3Az6l0fBohCEXVlGRWSQfnsXbQoC8fK1POtOzgNgQn2/7nz+ZaEMoDPVRA8m2pOIMUd9sf7Q6/xPTsNdDdbe1MZZC63sNVh+U1mQsPdc8K4DuiiRQ+nJj7D/MEVNfz1F0SQhCSi1XpWlIcA3sS9JqN1v9JOMILQyFYswIRNCcyQkvgN9rSF9FtXgo49gbaH9krKdNDcMlDhe+w/oqHSl55zt5pdV2qx3+9RhjJQ1DCojzGpO0= user@user-virtual-machine
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDbStlr50UpaQ45JRSRCbyLEs9kB+/68LByTWUcoOHdGuQW4+HBaSnzpPqg91nNlIII7vku1bAdUjN03QVYfvpffgit4zC949JChdf3NCto6r+GAS4vtTqY3d1TWGI3Fwiu7Qocw3EL6jfCy73VUut+1SkVBGBW/9ErSXltISqTbg5cgrOHjd+jHJmZ8t87SNy0IYbczEEsjrq7ksIUlH11X+ZKeL1mQISsDgs1mi6iZ/SZO/qzuzhVvT7c31OZN8+ypDRXDu4pk+AxYjZihE/YQ++qqw5GqQtkPZ/rsvtVPO7oKl3JwDl1jLw4EIedUQ/KzEsVthmsSbP+Lnuhar7f V360-deploy-keypair
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBM/5ijEsal4MKisBNdXfijRZw3bR/wMHWaNwD+L+6o2 evgeniy.maikov@streamlabs.ru

#  Change some default passwords
chpasswd:
  list: |
    root:VMzxc!123
#    user:user
  expire: False


#  Write to a log file (useing variables set in terraform) and show the ip on the console.
runcmd:
  - apt -y update
  - cd /home/ && curl -fsSL get.docker.com -o get-docker.sh && chmod +x get-docker.sh && ./get-docker.sh
  # - curl -fsSL get.docker.com
  # - chmod +x get-docker.sh
  # - ./get-docker.sh
  - cd /home/ && rm get-docker.sh
  #- sudo chmod ug+s /usr/bin/docker
  #- sudo gpasswd -a ubuntu docker
  - apt -y update
  - sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
  - sudo chmod +x /usr/local/bin/docker-compose
  - apt -y update
  #- sleep 10
  #- docker login -u gitlab+deploy-token-21 -p 1NFqJrTSBh6dNE3Kg5hG gitlab.streamlabs.ru:5000
  #- apt -y install gnupg2 wget vim
  #- sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
  #- wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
  #- apt -y update
  #- apt -y install postgresql-14
  - date >/root/cloudinit.log
  - hostnamectl set-hostname ${HOSTNAME}
  - echo ${HELLO} >>/root/cloudinit.log
  - echo "Done cloud-init" >>/root/cloudinit.log
  - ip a >/dev/tty1
  
